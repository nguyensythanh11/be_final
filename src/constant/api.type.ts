export class ResponseApi {
    statusCode: number;
    message: string;
    content: any;
    dateTime: Date;
    messageConstants: string;
    constructor(
        statusCode: number,
        message: string,
        content: any,
        dateTime: Date,
        messageConstants: string
    ){
        this.statusCode = statusCode,
        this.message = message,
        this.content = content,
        this.dateTime = dateTime,
        this.messageConstants = messageConstants
    }
}