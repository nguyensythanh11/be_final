import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { errorStatus } from 'src/error/error';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private roles: string[]){}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    let check = false;
    this.roles.forEach((role) => { 
      if(role == request.user.loai_nguoi_dung){
        check = true;
      }
    })
    if(check){
      return true;
    }
    throw errorStatus(401, 'Bạn không đủ quyền để thực hiện!');
  }
}
