import { Controller, Get, HttpCode, UseGuards, Query, Request, Body, Post, Param, Put, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ResponseApi } from 'src/constant/api.type';
import { AuthGuard } from '@nestjs/passport';
import { CapNhatUser, NguoiDungType } from 'src/auth/dao/auth.dao';
import { RolesGuard } from 'src/guard/roles.guard';

@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
@ApiTags('Quản lý Người Dùng')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @UseGuards(new RolesGuard(['admin']))
  @Get('/lay-danh-sach-nguoi-dung')
  @HttpCode(200)
  layDanhSachNguoiDung(): Promise<ResponseApi> {
    return this.userService.layDanhSachNguoiDung();
  };

  @UseGuards(new RolesGuard(['admin']))
  @Get('/lay-danh-sach-nguoi-dung-phan-trang')
  @HttpCode(200)
  layDanhSachNguoiDungPhanTrang(@Query('soTrang') soTrang: string, @Query('soPhanTuTrenTrang') soPhanTuTrenTrang: string): Promise<ResponseApi> {
    return this.userService.layDanhSachNguoiDungPhanTrang(Number(soTrang), Number(soPhanTuTrenTrang));
  };

  @UseGuards(new RolesGuard(['admin']))
  @Get('/tim-kiem-nguoi-dung')
  @HttpCode(200)
  timKiemNguoiDung(@Query('Từ tìm kiếm') tuTimKiem: string): Promise<ResponseApi> {
    return this.userService.timKiemNguoiDung(tuTimKiem);
  };

  @UseGuards(new RolesGuard(['admin']))
  @Get('/tim-kiem-nguoi-dung-phan-trang')
  @HttpCode(200)
  timKiemNguoiDungPhanTrang(@Query('Từ tìm kiếm') tuTimKiem: string, @Query('soTrang') soTrang: string, @Query('soPhanTuTrenTrang') soPhanTuTrenTrang: string): Promise<ResponseApi> {
    return this.userService.timKiemNguoiDungPhanTrang(tuTimKiem, Number(soTrang), Number(soPhanTuTrenTrang));
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-thong-tin-tai-khoan')
  @HttpCode(200)
  layThongTinTaiKhoan(@Request() req): Promise<ResponseApi> {
    let user = req.user;
    return this.userService.layThongTinTaiKhoan(user["tai_khoan"]);
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-thong-tin-nguoi-dung')
  @HttpCode(200)
  layThongTinNguoiDung(@Request() req): Promise<ResponseApi> {
    let user = req.user;
    return this.userService.layThongTinNguoiDung(user["tai_khoan"]);
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Put('/cap-nhat-thong-tin-nguoi-dung')
  @HttpCode(200)
  capNhatThongTinNguoiDung(@Request() req, @Body() updateInfo: CapNhatUser): Promise<ResponseApi> {
    let user = req.user;
    return this.userService.capNhatThongTinNguoiDung(user, updateInfo);
  };

  // Admin
  @UseGuards(new RolesGuard(['admin']))
  @Get('/lay-thong-tin-nguoi-dung/:ma_nguoi_dung')
  @HttpCode(200)
  layThongTinNguoiDungAdmin(@Param('ma_nguoi_dung') ma_nguoi_dung: string): Promise<ResponseApi> {
    return this.userService.layThongTinNguoiDungAdmin(Number(ma_nguoi_dung));
  };

  @UseGuards(new RolesGuard(['admin']))
  @Post('/them-nguoi-dung')
  @HttpCode(201)
  themNguoiDung(@Body() body: NguoiDungType): Promise<ResponseApi> {
    return this.userService.themNguoiDung(body);
  };

  @UseGuards(new RolesGuard(['admin']))
  @Put('/cap-nhat-thong-tin-nguoi-dung/:ma_nguoi_dung')
  @HttpCode(200)
  capNhatThongTinNguoiDungAdmin(@Param('ma_nguoi_dung') ma_nguoi_dung: string, @Body() updateInfo: CapNhatUser): Promise<ResponseApi> {
    return this.userService.capNhatThongTinNguoiDungAdmin(Number(ma_nguoi_dung), updateInfo);
  };

  @UseGuards(new RolesGuard(['admin']))
  @Delete('/xoa-nguoi-dung/:ma_nguoi_dung')
  @HttpCode(200)
  xoaNguoiDung(@Param('ma_nguoi_dung') ma_nguoi_dung: string): Promise<ResponseApi> {
    return this.userService.xoaNguoiDung(Number(ma_nguoi_dung));
  };
}
