import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { PrismaService } from 'src/prisma.service';
import { JwtStrategy } from 'src/strategy/jwt.strategy';
import { AuthService } from 'src/auth/auth.service';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from 'src/guard/roles.guard';

@Module({
  controllers: [UserController],
  providers: [UserService, PrismaService, JwtStrategy, AuthService]
})
export class UserModule {}
