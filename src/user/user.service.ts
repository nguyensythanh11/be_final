import { Injectable, HttpException } from '@nestjs/common';
import { ResponseApi } from 'src/constant/api.type';
import { PrismaService } from 'src/prisma.service';
import { NguoiDung } from '@prisma/client';
import { CapNhatUser, NguoiDungType } from 'src/auth/dao/auth.dao';
import * as bcrypt from 'bcrypt';
import { catchError, errorStatus } from 'src/error/error';

@Injectable()
export class UserService {
    constructor(private prisma: PrismaService){}

    async layDanhSachNguoiDung(): Promise<ResponseApi> {
        try {
            let data: NguoiDung[] = await this.prisma.nguoiDung.findMany();
            return new ResponseApi(
                200,
                'Xử lý thành công!',
                data,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception);
        }
    }

    async layDanhSachNguoiDungPhanTrang(soTrang: number, soPhanTuTrenTrang: number): Promise<ResponseApi> {
        try {
            let tongSoPhanTu = (await this.prisma.nguoiDung.findMany()).length;
            let soTrangToiDa = Math.ceil(tongSoPhanTu/soPhanTuTrenTrang);
            if(soTrang <= soTrangToiDa){
                let data =  await this.prisma.nguoiDung.findMany({
                    skip: (soTrang-1)*soPhanTuTrenTrang,
                    take: soPhanTuTrenTrang
                }); 
                return new ResponseApi(
                    200,
                    "Xử lý thành công!",
                    data,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async timKiemNguoiDung(tuTImKiem: string): Promise<ResponseApi> {
        try {
            let data: NguoiDung[] = await this.prisma.nguoiDung.findMany({
                where:{
                    ho_ten:{
                        contains: tuTImKiem
                    }
                }
            });
            if(data.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    data,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception)
        }
    }

    async timKiemNguoiDungPhanTrang(tuTImKiem: string, soTrang: number, soPhanTuTrenTrang: number): Promise<ResponseApi> {
        try {
            let data: NguoiDung[] = await this.prisma.nguoiDung.findMany({
                where:{
                    ho_ten:{
                        contains: tuTImKiem
                    }
                },
                skip: (soTrang-1)*soPhanTuTrenTrang,
                take: soPhanTuTrenTrang
            });
            if(data.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    data,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layThongTinTaiKhoan(tai_khoan: string): Promise<ResponseApi> {
        try {
            let user = await this.prisma.nguoiDung.findFirst({
                where: {
                    tai_khoan: tai_khoan
                },
                select: {
                    tai_khoan: true,
                    mat_khau: true
                }
            })
            return new ResponseApi(
                200,
                'Xử lý thành công!',
                user,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception);
        }
    }

    async layThongTinNguoiDung(tai_khoan: string): Promise<ResponseApi> {
        try {
            let user = await this.prisma.nguoiDung.findFirst({
                where: {
                    tai_khoan: tai_khoan
                },
                select: {
                    email: true,
                    ma_nguoi_dung: true,
                    ho_ten: true,
                    so_dt: true
                }
            })
            return new ResponseApi(
                200,
                'Xử lý thành công!',
                user,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception);
        }
    }

    async capNhatThongTinNguoiDung(user: NguoiDung, updateInfo: CapNhatUser): Promise<ResponseApi> {
        try {
            let checkEmail = await this.prisma.nguoiDung.findMany({
                where: {
                    AND: [
                        {
                            email: updateInfo.email
                        },
                        {
                            email: {
                                not: user.email
                            }
                        }
                    ]
                }
            })
            if(checkEmail.length > 0){
                throw errorStatus(409, 'Email đã tồn tại!')
            }
            let checkTaiKhoan = await this.prisma.nguoiDung.findMany({
                where: {
                    AND: [
                        {
                            tai_khoan: updateInfo.tai_khoan
                        },
                        {
                            tai_khoan: {
                                not: user.tai_khoan
                            }
                        }
                    ]
                }
            })
            if(checkTaiKhoan.length > 0){
                throw errorStatus(409, 'Tài khoản đã tồn tại!');
            }
            await this.prisma.nguoiDung.update({
                where: {
                    ma_nguoi_dung: user.ma_nguoi_dung
                },
                data: {
                    tai_khoan: updateInfo.tai_khoan,
                    email: updateInfo.email,
                    ho_ten: updateInfo.ho_ten,
                    so_dt: updateInfo.so_dt
                }
            })
            return new ResponseApi(
                200,
                'Cập nhật thành công!',
                updateInfo,
                new Date(),
                null
            )
        } catch (exception) {
            catchError(exception);
        }
    }

    // Admin
    async layThongTinNguoiDungAdmin(ma_nguoi_dung: number): Promise<ResponseApi> {
        try {
            let user = await this.prisma.nguoiDung.findUnique({
                where: {
                    ma_nguoi_dung: ma_nguoi_dung
                }
            })
            if(user){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    user,
                    new Date(),
                    null
                );
            } 
            throw errorStatus(404, 'Người dùng không tồn tại');
        } catch (exception) {
            catchError(exception);
        }
    }

    async themNguoiDung(body: NguoiDungType): Promise<ResponseApi> {
        try {
            let checkTaiKhoan = await this.prisma.nguoiDung.findFirst({
                where: {
                    tai_khoan: body.tai_khoan
                }
            });
            let checkEmail = await this.prisma.nguoiDung.findFirst({
                where: {
                    email: body.email
                }
            });
            if(checkTaiKhoan){
                throw errorStatus(409, 'Tài khoản đã tồn tại!');
            } else if(checkEmail){
                throw errorStatus(409, 'Email đã tồn tại');
            };
            let data = {
                ... body,
                mat_khau: bcrypt.hashSync(body.mat_khau, 10),
            };
            await this.prisma.nguoiDung.create({data});
            return new ResponseApi(
                201,
                "Thêm người dùng thành công!",
                data,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception);
        }
    }

    async capNhatThongTinNguoiDungAdmin(ma_nguoi_dung: number, updateInfo: CapNhatUser): Promise<ResponseApi> {
        try {
            let user = await this.prisma.nguoiDung.findUnique({
                where: {
                    ma_nguoi_dung
                }
            })
            let checkEmail = await this.prisma.nguoiDung.findMany({
                where: {
                    AND: [
                        {
                            email: updateInfo.email
                        },
                        {
                            email: {
                                not: user.email
                            }
                        }
                    ]
                }
            })
            if(checkEmail.length > 0){
                throw errorStatus(409, 'Email đã tồn tại');
            }
            let checkTaiKhoan = await this.prisma.nguoiDung.findMany({
                where: {
                    AND: [
                        {
                            tai_khoan: updateInfo.tai_khoan
                        },
                        {
                            tai_khoan: {
                                not: user.tai_khoan
                            }
                        }
                    ]
                }
            })
            if(checkTaiKhoan.length > 0){
                throw errorStatus(409, 'Tài khoản đã tồn tại!');
            }
            await this.prisma.nguoiDung.update({
                where: {
                    ma_nguoi_dung
                },
                data: {
                    tai_khoan: updateInfo.tai_khoan,
                    email: updateInfo.email,
                    ho_ten: updateInfo.ho_ten,
                    so_dt: updateInfo.so_dt
                }
            })
            return new ResponseApi(
                200,
                'Cập nhật thành công!',
                updateInfo,
                new Date(),
                null
            )
        } catch (exception) {
            catchError(exception);
        }
    }

    async xoaNguoiDung(ma_nguoi_dung: number): Promise<ResponseApi> {
        try {
            let checkUser = await this.prisma.nguoiDung.findUnique({
                where: {
                    ma_nguoi_dung
                }
            });
            if(checkUser){
                await this.prisma.nguoiDung.delete({
                    where: {
                        ma_nguoi_dung
                    }
                });
                return new ResponseApi(
                    200,
                    'Xóa thành công!',
                    {},
                    new Date(),
                    null
                )
            }
            throw errorStatus(404, 'Người dùng không tồn tại');
        } catch (exception) {
            catchError(exception);
        }
    }

}