import { HttpException, Injectable } from '@nestjs/common';
import { ResponseApi } from 'src/constant/api.type';
import { PrismaService } from 'src/prisma.service';
import { lichChieuType } from './dao/book-ticket.dao';
import { LichChieu } from '@prisma/client';
import { datVeType } from './dto/book-ticket.dto';
import { catchError } from 'src/error/error';

@Injectable()
export class BookTicketService {
    constructor(private prisma: PrismaService){}

    async layDanhSachPhongVe(ma_lich_chieu: number): Promise<ResponseApi> {
        try {
            let data = await this.prisma.lichChieu.findUnique({
                where: {
                    ma_lich_chieu
                },
                select: {
                    ma_lich_chieu: true,
                    RapPhim: {
                        select: {
                            CumRap: {
                                select: {
                                    ten_cum_rap: true,
                                    dia_chi: true,
                                }
                            },
                            ten_rap: true,
                            Ghe: {
                                select: {
                                    ma_ghe: true,
                                    ten_ghe: true,
                                    loai_ghe: true,
                                    DatVe: {
                                        where: {
                                            ma_lich_chieu
                                        },
                                        select: {
                                            NguoiDung: {
                                                select: {
                                                    tai_khoan: true,
                                                    ho_ten: true,
                                                    email: true,
                                                    so_dt: true
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    Phim: {
                        select: {
                            ten_phim: true,
                            hinh_anh: true
                        }
                    },
                    ngay_gio_chieu: true,
                    gia_ve: true
                }
            })
            const danhSachGhe = data.RapPhim.Ghe.map((ghe) => {
                let daDat: boolean = true; 
                if(ghe.DatVe.length > 0){
                    daDat = false;
                }
                return {
                    ... ghe,
                    daDat,
                    DatVe: undefined,
                    giaVe: data.gia_ve,
                    nguoiDat: ghe.DatVe[0]
                }
            })

            let res = { 
                thongTinPhim: {
                    ma_lich_chieu,
                    tenCumRap: data.RapPhim.CumRap.ten_cum_rap,
                    tenRap: data.RapPhim.ten_rap,
                    diaChi: data.RapPhim.CumRap.dia_chi,
                    tenPhim: data.Phim.ten_phim,
                    hinhAnh: data.Phim.hinh_anh,
                    ngayChieu: `${data.ngay_gio_chieu.getDate()}/${data.ngay_gio_chieu.getMonth() + 1}/${data.ngay_gio_chieu.getFullYear()}`,
                    gioChieu: `${data.ngay_gio_chieu.getHours()}:${String(data.ngay_gio_chieu.getMinutes()).padStart(2, '0')}:${String(data.ngay_gio_chieu.getSeconds()).padStart(2, '0')}`,
                    danhSachGhe
                }
            }
            return new ResponseApi(
                200,
                'Xử lý thành công!',
                res,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception);
        }
    }

    async taoLichChieu(lichChieu: lichChieuType): Promise<ResponseApi> {
        try {
            let data: LichChieu = {
                ma_phim: lichChieu.ma_phim,
                ma_rap: lichChieu.ma_rap,
                ngay_gio_chieu: new Date(lichChieu.ngay_gio_chieu),
                gia_ve: lichChieu.gia_ve,
                ma_lich_chieu: undefined
            };
            await this.prisma.lichChieu.create({data});
            return new ResponseApi(
                201,
                'Tạo lịch chiếu thành công!',
                data,
                new Date(),
                null
            )
        } catch (exception) {
            catchError(exception);
        }
    }

    async datVe(ve: datVeType, ma_nguoi_dung: number): Promise<ResponseApi> {
        try {
            let dataCreate = await Promise.all(ve.danhSachVe.map(async (veChiTiet) => {
                let checkGheDaDat = await this.prisma.datVe.findFirst({
                    where: {
                        ma_lich_chieu: ve.maLichChieu,
                        ma_ghe: veChiTiet.maGhe
                    }
                })
                if(checkGheDaDat){
                    throw new HttpException(
                        new ResponseApi(
                            409,
                            'Tài nguyên đã tồn tại!',
                            'Ghế trong suất chiếu này đã được đặt!',
                            new Date(),
                            null
                        ),
                        409
                    )
                }
                return {
                    ma_lich_chieu: ve.maLichChieu,
                    ma_ghe: veChiTiet.maGhe,
                    ma_nguoi_dung
                };
            }));
            await this.prisma.datVe.createMany({
                data: dataCreate
            })
            return new ResponseApi(
                201,
                'Đặt vé thành công',
                dataCreate,
                new Date,
                null
            )
        } catch (exception) {
            catchError(exception);
        }
    }
}


