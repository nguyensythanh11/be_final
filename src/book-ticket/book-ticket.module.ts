import { Module } from '@nestjs/common';
import { BookTicketService } from './book-ticket.service';
import { BookTicketController } from './book-ticket.controller';
import { JwtModule } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [BookTicketController],
  providers: [BookTicketService, JwtModule, PrismaService],
})
export class BookTicketModule {}
