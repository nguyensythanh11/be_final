import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsNumber } from "class-validator"

export class veChiTiet {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    maGhe: number
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    giaVe: number
}
export class datVeType {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    maLichChieu: number
    @ApiProperty({ type: [veChiTiet] })
    danhSachVe: veChiTiet[]
}