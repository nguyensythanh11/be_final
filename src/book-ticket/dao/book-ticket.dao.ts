import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber } from "class-validator";

export class lichChieuType {
    @ApiProperty({ default: 0, description: 'Mã rạp' })
    @IsNotEmpty()
    @IsNumber()
    ma_rap: number;
    @ApiProperty({ default: 0, description: 'Mã phim' })
    @IsNotEmpty()
    @IsNumber()
    ma_phim: number;
    @ApiProperty({ example: '2023-12-20 17:00:00', description: 'Ngày giờ chiếu', format: 'yyyy-mm-dd hh-mm-ss' })
    ngay_gio_chieu: string;
    @ApiProperty({ default: 0, description: 'Giá vé' })
    @IsNotEmpty()
    @IsNumber()
    gia_ve: number;
}
