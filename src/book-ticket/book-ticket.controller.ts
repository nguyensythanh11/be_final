import { Body, Controller, Get, HttpCode, Post, Query, Req, UseGuards } from '@nestjs/common';
import { BookTicketService } from './book-ticket.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { lichChieuType } from './dao/book-ticket.dao';
import { ResponseApi } from 'src/constant/api.type';
import { datVeType } from './dto/book-ticket.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/guard/roles.guard';

@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Quản lý đặt vé')
@Controller('book-ticket')
export class BookTicketController {
  constructor(private readonly bookTicketService: BookTicketService) {}

  @UseGuards(new RolesGuard(['admin']))
  @Get('/lay-danh-sach-phong-ve')
  @HttpCode(200)
  layDanhSachPhongVe(@Query('ma_lich_chieu') ma_lich_chieu: string): Promise<ResponseApi> {
    return this.bookTicketService.layDanhSachPhongVe(Number(ma_lich_chieu));
  }

  @UseGuards(new RolesGuard(['admin']))
  @Post('/tao-lich-chieu')
  @HttpCode(201)
  taoLichChieu(@Body() lichChieu: lichChieuType): Promise<ResponseApi> {
    return this.bookTicketService.taoLichChieu(lichChieu);
  }

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Post('/dat-ve')
  @HttpCode(201)
  datVe(@Req() req, @Body() ve: datVeType): Promise<ResponseApi> {
    let user = req.user;
    return this.bookTicketService.datVe(ve, user['ma_nguoi_dung']);
  }
}
