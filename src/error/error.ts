import { HttpException, InternalServerErrorException } from "@nestjs/common";
import { ResponseApi } from "src/constant/api.type";

export const catchError = (exception: any) => { 
    if (exception instanceof HttpException && exception.getStatus() !== 500) {
        throw exception;
    } else {
        throw new InternalServerErrorException('Lỗi server');
    }
}

export const errorStatus = (errorCode: number, content: string) => {
    let mess: string = "";
    if(errorCode == 404){
        mess = "Tài nguyên không tồn tại!";
    } else if(errorCode == 409){
        mess = "Tài nguyên đã tồn tại!";
    } else if(errorCode == 400){
        mess = "Xử lý thất bại!";
    } else if(errorCode == 401){
        mess = "Không được phân quyền!";
    }
    return new HttpException(
        new ResponseApi(
            errorCode,
            mess,
            content,
            new Date(),
            null
        ), 
        errorCode
    )
}