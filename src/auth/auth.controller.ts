import { Controller, Post, Body, HttpCode, Get, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiTags } from '@nestjs/swagger'
import { DangNhapType } from './dto/auth.dto';
import { ResponseApi } from 'src/constant/api.type';
import { DangKyType } from './dao/auth.dao';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/dang-nhap')
  @HttpCode(200)
  dangNhap(@Body() body: DangNhapType): Promise<ResponseApi> {
    return this.authService.dangNhap(body);
  }

  @Post('/dang-ky')
  @HttpCode(201)
  dangKy(@Body() body: DangKyType): Promise<ResponseApi>{
    return this.authService.dangKy(body);
  }

}