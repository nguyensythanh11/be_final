import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, MaxLength, MinLength, isEmail } from 'class-validator'

export class DangKyType {
    @ApiProperty()
    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(20)
    tai_khoan: string
    @ApiProperty()
    @IsNotEmpty()
    ho_ten: string
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string
    @ApiProperty()
    @IsNotEmpty()
    so_dt: string
    @ApiProperty()
    @IsNotEmpty()
    mat_khau: string
}

export class NguoiDungType extends DangKyType {
    @ApiProperty()
    @IsNotEmpty()
    loai_nguoi_dung: string
}

export class CapNhatUser {
    @ApiProperty()
    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(20)
    tai_khoan: string
    @ApiProperty()
    @IsNotEmpty()
    ho_ten: string
    @ApiProperty()
    @IsNotEmpty()
    email: string
    @ApiProperty()
    @IsNotEmpty()
    so_dt: string
}