import { Injectable, HttpException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResponseApi } from 'src/constant/api.type';
import { PrismaService } from 'src/prisma.service';
import { DangNhapType } from './dto/auth.dto';
import { DangKyType } from './dao/auth.dao';
import * as bcrypt from 'bcrypt';
import { catchError, errorStatus } from 'src/error/error'; 
import { jwtConstants } from './constants';

@Injectable()
export class AuthService {
    constructor(private jwtService: JwtService, private prisma: PrismaService){}

    async dangNhap(body: DangNhapType): Promise<ResponseApi> {
        try {
            const checkTaiKhoan = await this.prisma.nguoiDung.findFirst({
                where: {
                    tai_khoan: body.tai_khoan
                }
            });
            if(checkTaiKhoan){
                if(bcrypt.compareSync   (body.mat_khau, checkTaiKhoan.mat_khau)){
                    let accessToken = await this.jwtService.signAsync({
                        ... checkTaiKhoan,
                        mat_khau: undefined
                    })
                    let refreshToken = await this.jwtService.signAsync({
                        ... checkTaiKhoan,
                        mat_khau: undefined
                    },{
                        secret: jwtConstants.refresh_secret,
                        expiresIn: '7d'
                    })
                    return new ResponseApi(
                        200,
                        'Đăng nhập thành công!',
                        {
                            ... checkTaiKhoan,
                            mat_khau: undefined,
                            accessToken,
                            refreshToken
                        },
                        new Date(),
                        null
                    )
                } else {
                    throw errorStatus(404, 'Mật khẩu không đúng!');
                };
            }; 
            throw errorStatus(404, 'Tài khoản không đúng!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async dangKy(body: DangKyType): Promise<ResponseApi> {
        try {
            let checkTaiKhoan = await this.prisma.nguoiDung.findFirst({
                where: {
                    tai_khoan: body.tai_khoan
                }
            });
            let checkEmail = await this.prisma.nguoiDung.findFirst({
                where: {
                    email: body.email
                }
            });
            if(checkTaiKhoan){
                throw errorStatus(409, 'Tài khoản đã tồn tại!');
            } else if(checkEmail){
                throw errorStatus(409, 'Email đã tồn tại!');
            };
            let data = {
                ... body,
                mat_khau: bcrypt.hashSync(body.mat_khau, 10),
                loai_nguoi_dung: 'user',
            };
            await this.prisma.nguoiDung.create({data});
            return new ResponseApi(
                201,
                "Đăng ký thành công!",
                data,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception)
        }
    }
}
