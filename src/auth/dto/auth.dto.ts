import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator'

export class DangNhapType {
    @ApiProperty()
    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(20)
    tai_khoan: string
    @ApiProperty()
    @IsNotEmpty()
    mat_khau: string
};

export class DangNhapResponseType {
    ma_nguoi_dung: string
    tai_khoan: string
    ho_ten: string
    email: string
    so_dt: string
    loai_nguoi_dung: string
    accessToken: string
}
