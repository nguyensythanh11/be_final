import { Module } from '@nestjs/common';
import { CinemaService } from './cinema.service';
import { CinemaController } from './cinema.controller';
import { PrismaService } from 'src/prisma.service';
import { JwtStrategy } from 'src/strategy/jwt.strategy';

@Module({
  controllers: [CinemaController],
  providers: [CinemaService, PrismaService, JwtStrategy],
})
export class CinemaModule {}
