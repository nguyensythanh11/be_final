import { Controller, Get, HttpCode, Param, Query, UseGuards } from '@nestjs/common';
import { CinemaService } from './cinema.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ResponseApi } from 'src/constant/api.type';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/guard/roles.guard';

@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Quản lý rạp phim')
@Controller('cinema')
export class CinemaController {
  constructor(private readonly cinemaService: CinemaService) {}

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-thong-tin-he-thong-rap')
  @HttpCode(200)
  layThongTinHeThongRap(): Promise<ResponseApi> {
    return this.cinemaService.layThongTinHeThongRap();
  }

  @UseGuards(new RolesGuard(['admin']))
  @Get('/lay-thong-tin-he-thong-rap/:ma_he_thong_rap')
  @HttpCode(200)
  layThongTinHeThongRapChiTiet(@Param('ma_he_thong_rap') ma_he_thong_rap: string): Promise<ResponseApi> {
    return this.cinemaService.layThongTinHeThongRapChiTiet(Number(ma_he_thong_rap));
  }

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-thong-tin-cum-rap-theo-he-thong')
  @HttpCode(200)
  layThongTinCumRapTheoHeThong(@Query('ma_he_thong_rap') ma_he_thong_rap: string): Promise<ResponseApi> {
    return this.cinemaService.layThongTinCumRapTheoHeThong(Number(ma_he_thong_rap));
  }

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-thong-tin-lich-chieu-he-thong-rap')
  @HttpCode(200)
  layThongTinLichChieuHeThongRap(@Query('ma_he_thong_rap') ma_he_thong_rap: string): Promise<ResponseApi> {
    return this.cinemaService.layThongTinLichChieuHeThongRap(Number(ma_he_thong_rap));
  }

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('lay-thong-tin-lich-chieu-phim')
  @HttpCode(200)
  layThongTinLichChieuPhim(@Query('ma_phim') ma_phim: string): Promise<ResponseApi> {
    return this.cinemaService.layThongTinLichChieuPhim(Number(ma_phim));
  }
}
