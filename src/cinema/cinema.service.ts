import { HttpException, Injectable } from '@nestjs/common';
import { ResponseApi } from 'src/constant/api.type';
import { catchError, errorStatus } from 'src/error/error';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class CinemaService {
    constructor(private prisma: PrismaService){}

    async layThongTinHeThongRap(): Promise<ResponseApi> {
        try {
            let cinemaList = await this.prisma.heThongRap.findMany();
            return new ResponseApi(
                200,
                'Xử lý thành công',
                cinemaList,
                new Date(),
                null
            )
        } catch (exception) {
            catchError(exception);
        }
    }

    async layThongTinHeThongRapChiTiet(ma_he_thong_rap: number): Promise<ResponseApi> {
        try {
            let cinema = await this.prisma.heThongRap.findUnique({
                where: {
                    ma_he_thong_rap
                }
            });
            if(cinema){
                return new ResponseApi(
                    200,
                    'Xử lý thành công',
                    cinema,
                    new Date(),
                    null
                )
            }
            throw errorStatus(404, 'Hệ thống rạp phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layThongTinCumRapTheoHeThong(ma_he_thong_rap: number): Promise<ResponseApi> {
        try {
            let cumRapList = await this.prisma.cumRap.findMany({
                where: {
                    ma_he_thong_rap: ma_he_thong_rap
                },
                select: {
                    ma_cum_rap: true,
                    ten_cum_rap: true,
                    dia_chi: true,
                    RapPhim: {
                        select: {
                            ma_rap: true,
                            ten_rap: true
                        }
                    }
                },
                
            });
            if(cumRapList){
                return new ResponseApi(
                    200,
                    'Xử lý thành công',
                    cumRapList,
                    new Date(),
                    null
                )
            }
            throw errorStatus(404, 'Hệ thống rạp phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layThongTinLichChieuHeThongRap(ma_he_thong_rap: number): Promise<ResponseApi> {
        try {
            let lichChieuHeThongRap = await this.prisma.heThongRap.findUnique({
                where: {
                    ma_he_thong_rap
                },
                select: {
                    ma_he_thong_rap: true,
                    ten_he_thong_rap: true,
                    logo: true,
                    CumRap: {
                        select: {
                            ma_cum_rap: true,
                            ten_cum_rap: true,
                            dia_chi: true,
                            RapPhim: {
                                select: {
                                    ma_rap: true,
                                    ten_rap: true,
                                    LichChieu: {
                                        select: {
                                            Phim: true,
                                            ngay_gio_chieu: true,
                                            gia_ve: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
            if(lichChieuHeThongRap){
                return new ResponseApi(
                    200,
                    'Xử lý thành công',
                    lichChieuHeThongRap,
                    new Date(),
                    null
                )
            }
            throw errorStatus(404, 'Hệ thống rạp phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layThongTinLichChieuPhim(ma_phim: number): Promise<ResponseApi> {
        try {
            let lichChieuPhim = await this.prisma.heThongRap.findMany({
                select: {
                    ma_he_thong_rap: true,
                    ten_he_thong_rap: true,
                    logo: true,
                    CumRap: {
                        select: {
                            ma_cum_rap: true,
                            ten_cum_rap: true,
                            dia_chi: true,
                            RapPhim: {
                                select: {
                                    ma_rap: true,
                                    ten_rap: true,
                                    LichChieu: {
                                        where: {
                                            ma_phim
                                        },
                                        select: {
                                            Phim: true,
                                            ngay_gio_chieu: true,
                                            gia_ve: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
            if(lichChieuPhim){
                return new ResponseApi(
                    200,
                    'Xử lý thành công',
                    lichChieuPhim,
                    new Date(),
                    null
                )
            }
            throw errorStatus(404, 'Hệ thống rạp phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }
}
