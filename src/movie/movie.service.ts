import { Injectable, HttpException } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { ResponseApi } from 'src/constant/api.type';
import * as fs from 'fs';
import * as compress_images from 'compress-images';
import { ErrorCallback, CompletedCallback, Statistic } from 'compress-images';
import { Movie } from './dao/movie.dao';
import { formatISO } from 'date-fns';
import { catchError, errorStatus } from 'src/error/error';

@Injectable()
export class MovieService {
    constructor(private prisma: PrismaService){}
    
    async layDanhSachBanner(): Promise<ResponseApi> {
        try {
          let bannerList = await this.prisma.banner.findMany();
          return new ResponseApi(
            200,
            'Xử lý thành công!',
            bannerList,
            new Date(),
            null
          );
        } catch (exception) {
            catchError(exception);
        }
    }

    async layDanhSachPhim(): Promise<ResponseApi> {
        try {
            let movieList = await this.prisma.phim.findMany();
            return new ResponseApi(
                200,
                'Xử lý thành công!',
                movieList,
                new Date(),
                null
              );
        } catch (exception) {
            catchError(exception);
        }
    }

    async layDanhSachPhimPhanTrang(soTrang: number, soPhanTuTrenTrang: number): Promise<ResponseApi> {
        try {
            let movieList = await this.prisma.phim.findMany({
                skip: (soTrang-1)*soPhanTuTrenTrang,
                take: soPhanTuTrenTrang
            });
            if(movieList.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    movieList,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layDanhSachPhimTheoNgayKhoiChieu(ngayKhoiChieu: string): Promise<ResponseApi> {
        try {
            let validatedDate = validateAndTransformDate(ngayKhoiChieu);
            let movieList = await this.prisma.phim.findMany({
                where: {
                    ngay_khoi_chieu: validatedDate
                }
            })
            if(movieList.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    movieList,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layDanhSachPhimTheoNgayKhoiChieuPhanTrang(soTrang: number, soPhanTuTrenTrang: number, ngayKhoiChieu: string): Promise<ResponseApi> {
        try {
            let validatedDate = validateAndTransformDate(ngayKhoiChieu);
            let movieList = await this.prisma.phim.findMany({
                where: {
                    ngay_khoi_chieu: validatedDate
                },
                skip: (soTrang-1)*soPhanTuTrenTrang,
                take: soPhanTuTrenTrang
            })
            if(movieList.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    movieList,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async timKiemPhim(tenPhim: string): Promise<ResponseApi> {
        try {
            let movieList = await this.prisma.phim.findMany({
                where: {
                    ten_phim: {
                        contains: tenPhim
                    }
                },
            })
            if(movieList.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    movieList,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    } 

    async timKiemPhimPhanTrang(soTrang: number, soPhanTuTrenTrang: number,tenPhim: string): Promise<ResponseApi> {
        try {
            let movieList = await this.prisma.phim.findMany({
                where: {
                    ten_phim: {
                        contains: tenPhim
                    }
                },
                skip: (soTrang-1)*soPhanTuTrenTrang,
                take: soPhanTuTrenTrang
            })
            if(movieList.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    movieList,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    } 

    async layDanhSachPhimTheoNgay(tuNgay: string, denNgay: string): Promise<ResponseApi> {
        try {
            let validatedDateFrom = validateAndTransformDate(tuNgay);
            let validatedDateTo = validateAndTransformDate(denNgay);
            if(validatedDateTo < validatedDateFrom){
                throw errorStatus(400, 'Khoảng thời gian không đúng!');
            }
            let movieList = await this.prisma.phim.findMany({
                where: {
                    ngay_khoi_chieu: {
                        gte: validatedDateFrom,
                        lte: validatedDateTo
                    }
                }
            })
            if(movieList.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    movieList,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layDanhSachPhimTheoNgayPhanTrang(soTrang: number, soPhanTuTrenTrang: number,tuNgay: string, denNgay: string): Promise<ResponseApi> {
        try {
            let validatedDateFrom = validateAndTransformDate(tuNgay);
            let validatedDateTo = validateAndTransformDate(denNgay);
            if(validatedDateTo < validatedDateFrom){
                throw errorStatus(400, 'Khoảng thời gian không đúng!');
            }
            let movieList = await this.prisma.phim.findMany({
                where: {
                    ngay_khoi_chieu: {
                        gte: validatedDateFrom,
                        lte: validatedDateTo
                    }
                },
                skip: (soTrang-1)*soPhanTuTrenTrang,
                take: soPhanTuTrenTrang
            })
            if(movieList.length > 0){
                return new ResponseApi(
                    200,
                    'Xử lý thành công!',
                    movieList,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Trang không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }
    /**
     * --------------------------------------------------------
     */
    // Admin
    async uploadHinhAnhPhim(file: Express.Multer.File, ma_phim: number): Promise<ResponseApi> {
        try {
            let checkMovie = await this.prisma.phim.findFirst({
                where: {
                    ma_phim
                }
            });
            if(checkMovie){
                if(file.size > 50000){
                    optimizeImage(file);
                }
                const base64 = await convertToBase(file);
                await this.prisma.phim.update({
                    where: {
                        ma_phim
                    },
                    data: {
                        hinh_anh:'/public/image/' + file.filename
                    }
                })
                return new ResponseApi(
                    200,
                    'Cập nhật hình ảnh thành công!',
                    file,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async uploadTrailerPhim(file: Express.Multer.File, ma_phim: number): Promise<ResponseApi> {
        try {
            let checkMovie = await this.prisma.phim.findFirst({
                where: {
                    ma_phim
                }
            });
            if(checkMovie){
                await this.prisma.phim.update({
                    where: {
                        ma_phim
                    },
                    data: {
                        trailer:'/public/video/' + file.filename
                    }
                })
                return new ResponseApi(
                    200,
                    'Cập nhật hình ảnh thành công!',
                    file,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async themPhim(movie: Movie): Promise<ResponseApi> {
        try {
            let checkMovie = await this.prisma.phim.findFirst({
                where: {
                    ten_phim: movie.ten_phim
                }
            })
            if(checkMovie){
                throw errorStatus(409, 'Phim đã tồn tại!');
            }
            await this.prisma.phim.create({ data: {
                ... movie,
                ngay_khoi_chieu: formatISO(new Date(movie.ngay_khoi_chieu), { representation: 'complete' })
            } });
            return new ResponseApi(
                201,
                'Thêm phim thành công!',
                movie,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception);
        }
    }

    async capNhatPhim(movie: Movie, ma_phim: number): Promise<ResponseApi> {
        try {
            await this.prisma.phim.update({
                where: {
                    ma_phim
                },
                data: {
                    ... movie,
                    ngay_khoi_chieu: formatISO(new Date(movie.ngay_khoi_chieu), { representation: 'complete' })
                }
            })
            return new ResponseApi(
                201,
                'Thêm phim thành công!',
                movie,
                new Date(),
                null
            );
        } catch (exception) {
            catchError(exception);
        }
    }

    async xoaPhim(ma_phim: number): Promise<ResponseApi> {
        try {
            let checkMovie = await this.prisma.phim.findUnique({
                where: {
                    ma_phim
                }
            })
            if(checkMovie){
                await this.prisma.phim.delete({
                    where: {
                        ma_phim
                    }
                })
                return new ResponseApi(
                    200,
                    'Xóa phim thành công!',
                    'Đã xóa',
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }

    async layThongTinPhim(ma_phim: number): Promise<ResponseApi> {
        try {
            let checkMovie = await this.prisma.phim.findUnique({
                where: {
                    ma_phim
                }
            })
            if(checkMovie){
                return new ResponseApi(
                    200,
                    'Lấy thông tin thành công!',
                    checkMovie,
                    new Date(),
                    null
                );
            }
            throw errorStatus(404, 'Phim không tồn tại!');
        } catch (exception) {
            catchError(exception);
        }
    }
}

function validateAndTransformDate(dateString: string): Date {
    const validatedDate = new Date(dateString);
    if (isNaN(validatedDate.getTime())) {
      throw new Error('Invalid date format');
    }
    return validatedDate;
}

const convertToBase = async (file: Express.Multer.File) => { 
    let data = fs.readFileSync(process.cwd() + "/public/image/" + file.filename);
    let base64 = `data:${file.mimetype};base64,` + Buffer.from(data).toString("base64");
    return base64;
};

export const optimizeImage = async (file: Express.Multer.File) => {
    compress_images(
        process.cwd() + '/public/image/' + file.filename, 
        process.cwd() + '/public/file/', 
        { compress_force: false, statistic: true, autoupdate: true }, false,
        { jpg: { engine: "mozjpeg", command: ["-quality", "20"] } },
        { png: { engine: "pngquant", command: ["--quality=20-30", "-o"] } },
        { svg: { engine: "svgo", command: "--multipass" } },
        { gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] } },
        async function (error: ErrorCallback, completed: CompletedCallback, statistic: Statistic) {
            if (error) {
              console.error(error);
              return;
            }
            try {
              fs.unlinkSync(process.cwd() + "/public/image/" + file.filename);
              console.log('Old image file deleted successfully');
            } catch (unlinkError) {
              // Handle error during file deletion
              console.error('Error deleting old image file:', unlinkError);
            }
          }
    );
};


