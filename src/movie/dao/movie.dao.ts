import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, Max, Min } from "class-validator";

export class Movie {
    @ApiProperty()
    @IsNotEmpty()
    ten_phim: string
    @ApiProperty()
    @IsNotEmpty()
    trailer: string
    @ApiProperty()
    @IsNotEmpty()
    hinh_anh: string
    @ApiProperty()
    @IsNotEmpty()
    mo_ta: string
    @ApiProperty({
        format: 'yyyy-mm-dd',
        description: 'Example 2023-11-28',
        default: '2023-11-28'
    })
    ngay_khoi_chieu: Date
    @ApiProperty({
        description: '0 to 5 stars'
    })
    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    @Max(5)
    danh_gia: number
    @ApiProperty({
        description: 'true or false'
    })
    hot: boolean
    @ApiProperty({
        description: 'true or false'
    })
    dang_chieu: boolean
    @ApiProperty({
        description: 'true or false'
    })
    sap_chieu: boolean
}