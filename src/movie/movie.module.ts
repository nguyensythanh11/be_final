import { Module } from '@nestjs/common';
import { MovieService } from './movie.service';
import { MovieController } from './movie.controller';
import { PrismaService } from 'src/prisma.service';
import { JwtStrategy } from 'src/strategy/jwt.strategy';

@Module({
  controllers: [MovieController],
  providers: [MovieService, PrismaService, JwtStrategy],
})
export class MovieModule {}
