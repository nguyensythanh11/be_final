import { Controller, Get, HttpCode, Post, Query, UseInterceptors, UploadedFile, Param, Body, Put, Delete, UseGuards } from '@nestjs/common';
import { MovieService } from './movie.service';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { ResponseApi } from 'src/constant/api.type';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { UploadDto } from './dto/movie.dto';
import { Movie } from './dao/movie.dao';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/guard/roles.guard';

@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Quản lý Phim')
@Controller('movie')
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-danh-sach-banner')
  @HttpCode(200)
  layDanhSachBanner(): Promise<ResponseApi> {
    return this.movieService.layDanhSachBanner();
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-danh-sach-phim')
  @HttpCode(200)
  layDanhSachPhim(): Promise<ResponseApi> {
    return this.movieService.layDanhSachPhim();
  };
  
  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-danh-sach-phim-phan-trang')
  @HttpCode(200)
  layDanhSachPhimPhanTrang(@Query('soTrang') soTrang: string, @Query('soPhanTuTrenTrang') soPhanTuTrenTrang: string): Promise<ResponseApi> {
    return this.movieService.layDanhSachPhimPhanTrang(Number(soTrang),Number(soPhanTuTrenTrang));
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-danh-sach-phim-theo-ngay-khoi-chieu')
  @HttpCode(200)
  layDanhSachPhimTheoNgayKhoiChieu(
    @Query('ngayKhoiChieu') ngayKhoiChieu: string): Promise<ResponseApi> {
    return this.movieService.layDanhSachPhimTheoNgayKhoiChieu(ngayKhoiChieu);
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-danh-sach-phim-theo-ngay-khoi-chieu-phan-trang')
  @HttpCode(200)
  layDanhSachPhimTheoNgayKhoiChieuPhanTrang(@Query('soTrang') soTrang: string, @Query('soPhanTuTrenTrang') soPhanTuTrenTrang: string, @Query('ngayKhoiChieu') ngayKhoiChieu: string): Promise<ResponseApi> {
    return this.movieService.layDanhSachPhimTheoNgayKhoiChieuPhanTrang(Number(soTrang),Number(soPhanTuTrenTrang),ngayKhoiChieu);
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/tim-kiem-phim')
  @HttpCode(200)
  timKiemPhim(@Query('tenPhim') tenPhim: string): Promise<ResponseApi> {
    return this.movieService.timKiemPhim(tenPhim);
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/tim-kiem-phim-phan-trang')
  @HttpCode(200)
  timKiemPhimPhanTrang(@Query('soTrang') soTrang: string, @Query('soPhanTuTrenTrang') soPhanTuTrenTrang: string, @Query('tenPhim') tenPhim: string): Promise<ResponseApi> {
    return this.movieService.timKiemPhimPhanTrang(Number(soTrang),Number(soPhanTuTrenTrang),tenPhim);
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-danh-sach-phim-theo-ngay')
  @HttpCode(200)
  layDanhSachPhimTheoNgay(
    @Query('tuNgay') tuNgay: string, @Query('denNgay') denNgay: string): Promise<ResponseApi> {
    return this.movieService.layDanhSachPhimTheoNgay(tuNgay, denNgay);
  };

  @UseGuards(new RolesGuard(['admin', 'user']))
  @Get('/lay-danh-sach-phim-theo-ngay-phan-trang')
  @HttpCode(200)
  layDanhSachPhimTheoNgayPhanTrang(
    @Query('soTrang') soTrang: string, @Query('soPhanTuTrenTrang') soPhanTuTrenTrang: string, @Query('tuNgay') tuNgay: string, @Query('denNgay') denNgay: string): Promise<ResponseApi> {
    return this.movieService.layDanhSachPhimTheoNgayPhanTrang(Number(soTrang),Number(soPhanTuTrenTrang), tuNgay, denNgay);
  };

  @UseGuards(new RolesGuard(['admin']))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadDto
  })
  @Post('/upload-hinh-anh-phim/:ma_phim')
  @HttpCode(200)
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: process.cwd() + "/public/image",
      filename: (req,file,callback) => callback(null, new Date().getTime() + "_" + file.originalname)
    })
  }))
  uploadHinhAnhPhim(@UploadedFile() file: Express.Multer.File, @Param('ma_phim') ma_phim: string): Promise<ResponseApi> {
    return this.movieService.uploadHinhAnhPhim(file, Number(ma_phim));
  };

  @UseGuards(new RolesGuard(['admin']))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadDto
  })
  @Post('/upload-trailer-phim/:ma_phim')
  @HttpCode(200)
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: process.cwd() + "/public/video",
      filename: (req,file,callback) => callback(null, new Date().getTime() + "_" + file.originalname)
    })
  }))
  uploadTrailerPhim(@UploadedFile() file: Express.Multer.File, @Param('ma_phim') ma_phim: string): Promise<ResponseApi> {
    return this.movieService.uploadTrailerPhim(file, Number(ma_phim));
  };

  @UseGuards(new RolesGuard(['admin']))
  @Post('/them-phim')
  @HttpCode(201)
  themPhim(@Body() movie: Movie): Promise<ResponseApi> {
    return this.movieService.themPhim(movie);
  };
  
  @UseGuards(new RolesGuard(['admin']))
  @Put('/cap-nhat-phim/:ma_phim')
  @HttpCode(200)
  capNhapPhim(@Body() movie: Movie, @Param('ma_phim') ma_phim: string): Promise<ResponseApi> {
    return this.movieService.capNhatPhim(movie, Number(ma_phim));
  };

  @UseGuards(new RolesGuard(['admin']))
  @Delete('/xoa-phim/:ma_phim')
  @HttpCode(200)
  xoaPhim(@Param('id') ma_phim: string): Promise<ResponseApi> {
    return this.movieService.xoaPhim(Number(ma_phim));
  };

  @UseGuards(new RolesGuard(['admin']))
  @Get('/lay-thong-tin-phim/:ma_phim')
  @HttpCode(200)
  layThongTinPhim(@Param('ma_phim') ma_phim: string): Promise<ResponseApi> {
    return this.movieService.layThongTinPhim(Number(ma_phim));
  }
}
